package com.techuniversity;

import com.techuniversity.controller.EmpleadosController;
import com.techuniversity.utils.BadSeparator;
import com.techuniversity.utils.Utilidades;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.assertEquals;

@SpringBootTest
public class EmpleadosControllerTest {

    @Autowired
    EmpleadosController empleadosController;

    @Test
    public void testCadena(){
        String esperando = "L.U.Z D.E.L S.O.L";
        assertEquals(esperando, empleadosController.cadena("Luz del sol", "."));
    }

    @Test
    public void testBadSeparator(){
        try{
            Utilidades.getCadena("Javi panes", "..");
        }catch (BadSeparator bs){
            bs.getMessage();
        }
    }
}
