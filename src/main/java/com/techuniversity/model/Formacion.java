package com.techuniversity.model;

public class Formacion {
    private String fecha;
    private String titulo;

    public Formacion() {
    }

    public Formacion(String fecha, String titulo) {
        this.fecha = fecha;
        this.titulo = titulo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    @Override
    public String toString() {
        return "Formacion{" +
                "fecha='" + fecha + '\'' +
                ", titulo='" + titulo + '\'' +
                '}';
    }
}
