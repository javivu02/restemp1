package com.techuniversity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestEmp1Application {

	public static void main(String[] args) {
		SpringApplication.run(RestEmp1Application.class, args);
	}

}
