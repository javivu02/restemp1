package com.techuniversity.controller;

import com.techuniversity.model.Empleado;
import com.techuniversity.model.Empleados;
import com.techuniversity.model.Formacion;
import com.techuniversity.repository.EmpleadosDAO;
import com.techuniversity.utils.Configuracion;
import com.techuniversity.utils.Utilidades;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/empleados")
public class EmpleadosController {

    @Autowired
    private EmpleadosDAO empleadoDao;

    @GetMapping(path="/", produces = "application/json")
    public Empleados getEmpleados() {
        return empleadoDao.getAllEmpleados();
    }

    @GetMapping(path="/{id}")
    public ResponseEntity<Empleado> getEmpleado(@PathVariable int id){
        Empleado emp = empleadoDao.getEmpleado(id);
        if (emp != null) return new ResponseEntity<>(emp, HttpStatus.OK);
        else return ResponseEntity.notFound().build();
    }

    @PostMapping(path = "/", consumes="application/json", produces = "application/json")
    public ResponseEntity<Object> addEmpleado(@RequestBody Empleado emp){
        Integer id = empleadoDao.getAllEmpleados().getListaEmpleados().size() +1;
        emp.setId(id);
        empleadoDao.addEmpleado(emp);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(emp.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping(path = "/", consumes="application/json", produces = "application/json")
    public ResponseEntity<Object> updEmpleado(@RequestBody Empleado emp){
        Empleado modEmpleado = empleadoDao.updEmpleado(emp);
        if (modEmpleado == null){
            return ResponseEntity.notFound().build();
        }else{
            return ResponseEntity.ok().build();
        }
    }

    @PutMapping(path = "/{id}", consumes="application/json", produces = "application/json")
    public ResponseEntity<Object> updEmpleado(@PathVariable int id,@RequestBody Empleado emp){
        Empleado modEmpleado = empleadoDao.updEmpleado(id, emp);
        if (modEmpleado == null){
            return ResponseEntity.notFound().build();
        }else{
            return ResponseEntity.ok().build();
        }
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Object> deleteEmpleado (@PathVariable int id){
        empleadoDao.deleteEmpleado(id);
        return ResponseEntity.ok().build();
    }

    @PatchMapping(path = "/{id}", consumes="application/json", produces = "application/json")
    public ResponseEntity<Object> patchEmpleado (@PathVariable int id, @RequestBody Map<String, Object> updates){
        Empleado emp = empleadoDao.softUpdEmpleado(id, updates);
        if (emp == null){
            return ResponseEntity.notFound().build();
        }else{
            return ResponseEntity.ok().build();
        }
    }

    @GetMapping(path = "/{id}/formaciones",  produces = "application/json")
    public ResponseEntity<List<Formacion>> getFormacionesEmpleado (@PathVariable int id){
        List<Formacion> formaciones = empleadoDao.getFormacionEmpleado(id);
        if (formaciones == null){
            return ResponseEntity.notFound().build();
        }else{
            return ResponseEntity.ok().body(formaciones);
        }
    }

    @PostMapping(path = "/{id}/formaciones", consumes="application/json", produces = "application/json")
    public ResponseEntity<Object> addFormacionEmpleado(@PathVariable int id,@RequestBody Formacion formacion){
        Empleado emp = empleadoDao.addFormacionEmpleado(id, formacion);
        if (emp == null){
            return ResponseEntity.notFound().build();
        }else{
            return ResponseEntity.ok().build();
        }
    }

    @Value("${app.titulo")
    private String titulo;
    @GetMapping("/titulo")
    public String getTitulo(){
        return titulo;
    }

    @Autowired
    private Environment environment;
    @GetMapping("/prop")
    public String gePropiedad(@RequestParam("key") String key){
        String valor = "Sin Valor";
        String keyValor = environment.getProperty(key);
        if((keyValor != null ) && !keyValor.isEmpty()) {
            return valor;
        }
        return keyValor;
    }

    @Autowired
    private Configuracion config;
    @GetMapping("/autor")
    public String getAutor(){
        return getAutor();
    }


    @RequestMapping("/home")
    public String home(){
        return "entra";
    }


    @GetMapping("/cadena")
    public String cadena(@RequestParam String texto, @RequestParam String separador){
        try{
            return Utilidades.getCadena(texto, separador);
        }catch (Exception e){
            return e.getMessage();
        }
    }


}